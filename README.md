# OpenML dataset: ELE-1

https://www.openml.org/d/42361

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Electrical Length data set

This problem with only two input variables involves a small search space (small complexity). However, it is still an interesting problem since the system is strongly nonlinear and the available data is limited to a low number of examples presenting noise. All of these drawbacks make the modeling surface complicated indeed and, in this case, produce a strong overfitting of the obtained models.

### Attributes
      1. Inhabitants - integer [1, 320]
      2. Distance - real [60.0, 1673.329956]
      3. Length - real [80.0, 7675.0]

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42361) of an [OpenML dataset](https://www.openml.org/d/42361). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42361/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42361/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42361/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

